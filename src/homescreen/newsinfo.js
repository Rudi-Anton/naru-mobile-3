import React, { Component } from 'react';
import Expo from "expo";
import { View, StyleSheet, AsyncStorage, TouchableOpacity } from 'react-native';
import { Thumbnail, Text, Body, Left, Right, List, ListItem } from 'native-base';

export default class Newsinfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      property: this.props,
      dataNews: [],
      isReady: false,
    }
  }
  componentDidMount() {
    const baseUrl = "http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/news_event_top";
    fetchData(baseUrl).then(data => {
      this.setState({ dataNews: data, isReady: true });
    });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    } else {
      return(
        <List style={{backgroundColor:"#ffff"}}>
          <ListContent news={this.state}/>
        </List>
      );
    }
  }

}

function fetchData(url) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("news", (error, result) => {
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
      .then(response => response.json())
      .then((data) => {
        resolve(data);
      })
      .catch((e) => {
        reject(e);
      })
    })
  })
}

function ListContent(props) {
  const news = props.news.dataNews;
  if (typeof news == 'undefined') {
    return (
      <Text style={styles.wordTitle}>Tidak ada berita</Text>
    )
  }
  return (
    <View>
      {
        news.length == 0 ?
        (
          <Text style={styles.wordTitle}>Tidak ada berita</Text>
        ) : (
          <List>
            {news.map((item, idx) => {
              return (
                <TouchableOpacity onPress={() => props.news.property.navigation.navigate("NewsDetail", { idBerita : item._id })}>
                  <View style={{flex: 1, flexDirection: 'row',borderColor: '#f8f8f8', borderBottomWidth: 2}}>
                    <Thumbnail square source={{uri: item.newsImage}} style={styles.imageBerita} />
                    <Body style={styles.wordBerita}>
                      <Text style={styles.wordTitle}>{sliceNewsTitle(item.newsTitle)}</Text>
                      <Text style={styles.wordDate}>{parseDate(item.newsStartDate)}</Text>
                      <Text style={styles.wordDesc}>{sliceNewsDescription(item.newsDescription)}</Text>
                    </Body>
                  </View>
                </TouchableOpacity>
              )
            })}
          </List>
        )
      }
    </View>
  )
}

function parseDate(data) {
  if (!data) return "";
  const date = new Date(data);
  let retval = "";
  switch (date.getDay()) {
    case 0: retval+= "Senin"; break;
    case 1: retval+= "Selasa"; break;
    case 2: retval+= "Rabu"; break;
    case 3: retval+= "Kamis"; break;
    case 4: retval+= "Jumat"; break;
    case 5: retval+= "Sabtu"; break;
    case 6: retval+= "Minggu"; break;
  }
  retval += ", " + date.getDate();
  switch (date.getMonth()) {
    case 0: retval+= " Jan"; break;
    case 1: retval+= " Feb"; break;
    case 2: retval+= " Mar"; break;
    case 3: retval+= " Apr"; break;
    case 4: retval+= " Mei"; break;
    case 5: retval+= " Jun"; break;
    case 6: retval+= " Jul"; break;
    case 7: retval+= " Agu"; break;
    case 8: retval+= " Sep"; break;
    case 9: retval+= " Okt"; break;
    case 10: retval+= " Nov"; break;
    case 11: retval+= " Des"; break;
  }
  retval += " " + date.getFullYear();
  return retval;
}

function sliceNewsTitle(text) {
  return text.length > 50 ? text.substring(0, 50) + "..." : text;
}

function sliceNewsDescription(text) {
  return text.length > 60 ? text.substring(0, 60) + "..." : text;
}

const styles = StyleSheet.create({
    imageBerita: {
      borderRadius:3,
      margin:"3%",
      width: 100,
      height: 70,
    },
    wordBerita: {
      alignItems: 'flex-start',
      margin:"2%",
    },
    wordDate: {
      fontFamily:'Ubuntu_Light',
      fontSize: 10,
      color:"#363636",
    },
    wordTitle: {
      fontFamily: 'Ubuntu_Regular',
      fontSize: 14,
    },
    wordDesc: {
      marginTop:'2%',
      fontFamily:'Ubuntu_Light',
      fontSize: 12,
    },
    
  });