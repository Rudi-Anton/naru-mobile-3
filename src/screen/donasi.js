import React from "react";
import { StatusBar, Image, MapView, Alert, AsyncStorage, WebView , Modal, View , TouchableOpacity , StyleSheet, Linking} from "react-native";
import Expo from "expo";
import {
    Container,
    Header,
    Content,
    Form,
    Left,
    Right,
    Body,
    Title,
    Button,
    Icon,
    Item,
    Input,
    Card,
    CardItem,
    Label, 
    ListItem, 
    CheckBox, 
    Text, Avatar 
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index";

export default class Donasi extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      idNews: "",
      isReady: true,
      nama: "",
      email:"",
      nominal:"",
      phonenumber:"",
      payment: "" 
    }
  }
  

  render() {
    
    return (
      <Container>
        <AppHeader title="Donasi" />
        <Content>
        <Form style={{ backgroundColor: 'white' }}> 
          
          <Label>Tulis Jumlah Donasi Anda</Label>
          <Item regular>
            <Input onChangeText={this.handleNominal}  placeholder="Rp. 0"/>
          </Item>

          <Label>Harap Memasukan Identitas Anda</Label>
          <ListItem> 
          <Left>
              <Image style={{width: 150}} source={require("../../img/asset/donasi/facebook.png")} resizeMode='contain' />
          </Left>
          <Right>
              <Image style={{width: 150}} source={require("../../img/asset/donasi/google.png")} resizeMode='contain' />
          </Right> 
          </ListItem>

          <Label>Atau</Label>
          <ListItem>
              <Input onChangeText={this.handleNama} placeholder="Nama Lengkap"/>
              <Input onChangeText={this.handleEmail} placeholder="Alamat Email"/>
          </ListItem>

          <Label>Dan nomor ponsel anda untuk menerima SMS status donasi :</Label>
          <Item>
              <Input onChangeText={this.handlePhoneNumber} placeholder="081xxx atau 62xxx (angka saja)"/>
          </Item>

          <Label>Pilih alat pembayaran :</Label>
  
            <Item>
                <Left> 
                 <Button onPress={() => this.setState({payment: 'T'})} value='T-money' style={{ backgroundColor: 'white' }}>
                    <Image style={{width: 150}} source={require("../../img/asset/donasi/t-money.png")} resizeMode='contain'/> 
                  </Button>
                </Left>
                <Right>
                  <Button onPress={() => this.setState({payment: 'CC'})} value='CC' style={{ backgroundColor: 'white' }}>
                    <Image style={{width: 150}} source={require("../../img/asset/donasi/cc-card.png")} resizeMode='contain' />
                  </Button>
                </Right>
            </Item>

            <ListItem>
                <CheckBox checked={true} />
                <Body>
                  <Text >Saya percaya dan setuju dengan Syarat & Ketentuan Donasi dalam aplikasi ini.</Text>
                </Body>
            </ListItem>

            <Button block onPress={this.posesDonasi}>
               <Text uppercase={false}>Transfer Sekarang</Text>
            </Button>

          </Form>
        </Content> 
      </Container>
    );
  }
  
    handleNama = (text) => {
      this.setState({ nama: text })
    }
    handleEmail = (text) => {
        this.setState({ email: text })
    }
    handleNominal = (text) => {
      this.setState({ nominal: text })
    }
    handlePhoneNumber = (text) => {
        this.setState({ phonenumber: text })
    }
    handlePayment = (text) => {
        this.setState({ payment: text })
    }
 
    posesDonasi = () => {   
        if (this.state.nama == null || this.state.nama == "" ||   this.state.email == null || this.state.email == "" || 
        this.state.nominal == null || this.state.nominal == "" || this.state.payment == null || this.state.payment == "" || 
        this.state.phonenumber == null || this.state.phonenumber == "" ) {
            Alert.alert(
                'Pesan',
                'Nama / Email / Nominal / Nomer Telpon tidak boleh kosong',
                [ 
                    { text: 'OK', style: 'cancel' },
                ],
                { cancelable: false }
            )
        } else {
  
          if(this.state.payment=='T'){        
                //T-Money
                return fetch("http://ec2-54-255-179-65.ap-southeast-1.compute.amazonaws.com:9000/api/tmoney/donation", {
                  method: 'POST',
                  headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                  },
                  body: JSON.stringify({
                      email: this.state.email,
                      amount: this.state.nominal,
                      no_hp : this.state.phonenumber,
                      nama_lengkap : this.state.nama,
                      password : "Ranger12!",
                      pin : "582876",
                      tujuan : "TMONEYDDWKF",
                      idNews : "10"
                  })
              })

              .then(response => response.json())
              .then((data) => { 
                 
                  AsyncStorage.setItem("hasil", data.data.status); 
                  AsyncStorage.getItem("hasil", (error, result) => {
                      if (result) {
                          //console.log("result : " + result)

                      }
                      if (result == null||result=="") { 
                          Alert.alert(
                              'Pesan',
                              'Anda tidak terdaftar di Account T-Money',
                              [
                                
                                  { text: 'OK', style: 'cancel' },
                              ],
                              { cancelable: false }
                          )
                      } else {
                          Alert.alert(
                              'Pesan',
                              'Transfer Berhasil',
                              [ 
                                {
                                    text: 'OK', onPress: () => {
                                        this.props.navigation.navigate("Promo");
                                        this.setState(this.state);
                                    }
                                },
                              ],
                              { cancelable: false }
                          )
                      }
                  })

              })
              .catch(error => {
                  Alert.alert(
                      'Pesan',
                      'Anda tidak terdaftar di Account T-Money',
                      [ 
                          { text: 'OK', style: 'cancel' },
                      ],
                      { cancelable: false }
                  )
              });
          }else{
            //Alert.alert(this.state.payment);   
              
            var param = {
                store: "19509",
                auth_key: "nkDCS-WLZg^XkzTB", 
                amount : this.state.nominal,
                name : this.state.nama, 
                idNews : "10"
            };
            var formBody = [];
            for (var property in param) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(param[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");
   
          //Fin Pay CC
            return fetch("http://ec2-54-255-179-65.ap-southeast-1.compute.amazonaws.com:9000/api/finpaycc/donation-mainapi", {
                method: 'POST',
                headers: { 
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                },
                body: formBody
            }) 
              .then(response => response.json())
              .then((data) => {  
                  
                  AsyncStorage.setItem("link", data.data.order.url); 
                  AsyncStorage.getItem("link", (error, result) => {
                      if (result) {
                          //console.log("result : " + result) 
                      }
                      if (result == null||result=="") { 
                          Alert.alert(
                              'Pesan',
                              'Anda tidak terdaftar di Account CC Finpay',
                              [ 
                                  { text: 'OK', style: 'cancel' },
                              ],
                              { cancelable: false }
                          )
                      } else { 
                        //Alert.alert(data.data.order.url);
                        
                        const uri = data.data.order.url;
                        
                        Linking.canOpenURL(uri).then(supported => {
                            if (!supported) {
                              //console.log('Can\'t handle url: ' + uri);
                            } else {
                              return Linking.openURL(uri);
                              Alert.alert(
                                    'Pesan',
                                    'Silakan Anda menlanjutkan transaksi pada CC Finpay',
                                    [ 
                                        { text: 'OK', style: 'cancel' },
                                    ],
                                    { cancelable: false }
                                )
                            }
                          }).catch(err => console.error('An error occurred', err));
                      }
                  })

              })
              .catch(error => {
                  Alert.alert(
                      'Pesan',
                      'Anda tidak terdaftar di Account CC Finpay',
                      [ 
                          { text: 'OK', style: 'cancel' },
                      ],
                      { cancelable: false }
                  )
              });
           }
           
        }
    }
}
