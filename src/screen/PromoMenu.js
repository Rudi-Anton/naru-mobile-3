import React from "react";
import { Image } from "react-native";
import {
    Container,
    Content,
    Button,
    Thumbnail,
    Grid,
    Row,
    Col
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index"

export default class PromoMenu extends React.Component {
  render() {
    return (
      <Container>
        <AppHeader title="Promo" />
        <Content>
          <Grid>
            <Row>
              <Col style={{height: 250, justifyContent: 'flex-end'}}>
                <Button style={{height: 150, width: 150, alignSelf: 'center'}} transparent onPress={() => this.props.navigation.navigate("Promo")}>
                  <Image style={{height: 150, width: 150}} source={require("../../img/asset/promo/group_4.png")} />
                </Button>
              </Col>
              <Col style={{height: 250, justifyContent: 'flex-end'}}>
                <Button style={{height: 150, width: 150, alignSelf: 'center'}} transparent onPress={() => this.props.navigation.navigate("Peta")}>
                  <Image style={{height: 150, width: 150}} source={require("../../img/asset/promo/group_3.png")} />
                </Button>
              </Col>
            </Row>
            <Row>
              <Col style={{height: 250, justifyContent: 'center'}}>
                <Button style={{height: 150, width: 150, alignSelf: 'center'}} transparent onPress={() => this.props.navigation.navigate("Zakat")}>
                  <Image style={{height: 150, width: 150}} source={require("../../img/asset/promo/group.png")} />
                </Button>
              </Col>
              <Col style={{height: 250, justifyContent: 'center'}}>
                <Button style={{height: 150, width: 150, alignSelf: 'center'}} transparent onPress={() => this.props.navigation.navigate("Donasi")}>
                  <Image style={{height: 150, width: 150}} source={require("../../img/asset/promo/group_2.png")} />
                </Button>
              </Col>
            </Row>
          </Grid>
        </Content>
        <AppFooter navigation={this.props.navigation} />
      </Container>
    );
  }
    
  go(param) {
    if (param == "promo") this.props.navigation.navigate("Promo");
  }
};