import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Expo, { Font } from "expo";

import Swiper from '../homescreen/swiper';
import Newsinfo from '../homescreen/newsinfo';
import Kuliner from '../homescreen/kuliner';
import Jadwal from '../homescreen/jadwal';

import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Body, Title, Thumbnail, Input, Item } from 'native-base';
import { AppHeader, AppFooter } from '../app-nav/index';

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      <Container>
        <AppHeader isHome />
        <Header searchBar rounded style={{ backgroundColor: 'white' }}>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Cari" />
          </Item>
        </Header>
        <Content style={{}}>
          <Swiper />
          <View style={styles.infoBerita}>
            <Text style={styles.infoText}>Info Terkini</Text>
          </View>
          <View style={styles.panelBerita}>
            <Newsinfo navigation={this.props.navigation} />
          </View>
          <View style={styles.infoBerita}>
            <Text style={styles.infoText}>Rekomendasi Kuliner Di Sekitar Anda</Text>
          </View>
          <View style={styles.panelBerita}>
            <Kuliner />
          </View>
          <View style={styles.infoSholat}>
            <Text style={styles.infoText}>Jadwal Sholat</Text>
          </View>
          <View style={styles.panelBerita}>
            <Jadwal />
          </View>
        </Content>
        <AppFooter navigation={this.props.navigation} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  infoBerita: {
    paddingTop: "5%",
    alignItems: "center",
    backgroundColor: "#ffffff",
    paddingBottom: "5%",
  },
  infoSholat: {
    alignItems: "center",
    paddingLeft: "5%",
    paddingTop: "10%",
    backgroundColor: "#ffffff",
    paddingBottom: "5%",
  },
  infoText: {
    fontFamily: 'Ubuntu_Light',
  },
  imageBerita: {
    borderRadius: 3,
    marginLeft: "2%",
    marginTop: "2%",
    width: "33%",
    height: "10%"
  },
  panelBerita: {
    backgroundColor: "#f8f8f8",
    padding: "3%",
  }
});